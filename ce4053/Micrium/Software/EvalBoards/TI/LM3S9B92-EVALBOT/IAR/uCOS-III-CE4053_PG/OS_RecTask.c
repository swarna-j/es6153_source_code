#ifndef OS_RECTASK_C
    #define OS_RECTASK_C
#endif

//------------------Includes--------------------------
#include <os.h>
#include <math.h>

//-------------------Profiling------------------------
#if OS_CFG_TRACE_EN > 0u 
static CPU_TS StartTime, StartTime1;
static CPU_TS ScheduleStart;
static CPU_FP32 taskScheduleOverhead;
static CPU_TS DSInsertStart;
#endif

//-------------------Typedefs-------------------------
#define NUM_OF_TASKS 10

typedef struct taskInfo {
  OS_TCB *p_tcb;
  OS_TCB p_tcb1;
  CPU_INT32U period;
  CPU_INT32U nextReleaseTime;
}TaskInfo;

typedef enum { RED, BLACK } Color;
typedef struct t_red_black_node Node;

struct t_red_black_node {
    Color color;    
    Node *next;
    TaskInfo st_node;
    Node *left, *right, *parent;
    CPU_INT32U isHead;
    CPU_INT32U isTail;
    CPU_INT32U Overflow_Flag;
};

struct t_red_black_node Nodes[NUM_OF_TASKS];
typedef  struct t_red_black_node RBTree;

CPU_INT32U i = 0, j = 0;
static RBTree *Tree;

typedef enum {false = 0, true} bool; 
typedef struct fibo_node fibonode;

struct fibo_node {
	fibonode* left;
        fibonode * next;
	fibonode* right;
	fibonode* child;
	fibonode* parent;
	TaskInfo sched_val;
        CPU_INT32U isHead;
        CPU_INT32U isTail;
	CPU_INT32U degree;
	bool mark;  
        CPU_CHAR C;
};

CPU_INT32U heap_count;
//typedef struct FibonacciHeap {
fibonode* heap;
//};
fibonode heap_node[NUM_OF_TASKS];

//------------------------------------------------------------------------------------------------------------------------
//                                                  FUNCTION PROTOTYPES
//------------------------------------------------------------------------------------------------------------------------

void          RBTreeRotateLeft        (Node *pt);
void          RBTreeRotateRight       (Node *pt);
void          RBTreeInsert            (TaskInfo data);                                         
void          TreeFind                (CPU_INT32U currTick);
void          RecRdyListInsert        (OS_TCB* p_tcb);
RBTree*       BSTInsert               (RBTree *Tree, Node *pt);
RBTree*       RBT_SearchNode          (RBTree* tree, CPU_INT32U currTick );  
RBTree*       RBT_RemoveNode          (CPU_INT32U currTick );
CPU_INT32U    findLength              (RBTree* tree);
RBTree*       listTraverse            (RBTree* tree);
void         Update_Release_Time     (RBTree* root);
void          overflow_handler        ();

//---------Heap Utility Functions -------       
fibonode* InitializeHeap();
CPU_INT32U Fibonnaci_link(fibonode*, fibonode*, fibonode*);
fibonode* Create_node(TaskInfo tree_node);
fibonode* insert(fibonode *, fibonode *);
fibonode* Extract_Min();
CPU_INT32U Consolidate(fibonode *);
fibonode* heap_insert(TaskInfo tree_node);
fibonode* find(fibonode* heap, CPU_INT32U period) ;
fibonode* heaplistTraverse(fibonode* heap) ;
CPU_INT32U heapfindLength(fibonode* heap) ;

//------------------------------------------------------------------------------------------------------------------------
//                                                   FUNCTION DEFINITIONS
//------------------------------------------------------------------------------------------------------------------------
// A utility function to insert a new node with given key in BST 
RBTree* BSTInsert(RBTree *tree, Node *pt)
{    
  RBTree* Check = RBT_SearchNode(tree, (pt->st_node).nextReleaseTime); // If the tree is empty, return a new node 
  if (Check == NULL)
  {
    if (tree == NULL)
    {       
        tree = pt;
        tree->isHead = 1;
        tree->isTail = 1;
        tree->next = NULL;
    }
    // Otherwise, recur down the tree 
    if ((pt->st_node).nextReleaseTime < (tree->st_node).nextReleaseTime)
    {
        tree->left  = BSTInsert(tree->left, pt);
        tree->left->parent = tree;
    }
    else if ((pt->st_node).nextReleaseTime > (tree->st_node).nextReleaseTime)
    {
        tree->right = BSTInsert(tree->right, pt);
        tree->right->parent = tree;
    }
  }
  else
  { 
      Node *ptr;
      ptr = listTraverse(Check);
      ptr->next = pt;
      ptr->isTail = 0;
      pt->isTail = 1;    
      pt->isHead = 0; 
      pt->left = pt->right = pt->parent = NULL;
      pt->next = NULL;
      //Check = treeSort(Check);
   }
    // return the (unchanged) node pointer 
    return tree;
}
 
void RBTreeRotateLeft(Node *pt)
{    
    Node *rightchild = pt->right;
    pt->right = rightchild->left;
 
    if (pt->right != NULL)
        pt->right->parent = pt;
 
    rightchild->parent = pt->parent;
 
    if (pt == Tree)
    {
        Tree = rightchild;
    }
    else 
    {
      if (pt == pt->parent->left)
        pt->parent->left = rightchild;
      else
        pt->parent->right = rightchild;
    }
    rightchild->left = pt;
    pt->parent = rightchild;
}
 
void RBTreeRotateRight( Node *pt)
{
    Node *leftchild = pt->left;
    pt->left = leftchild->right;
 
    if (pt->left != NULL)
        pt->left->parent = pt;
 
    leftchild->parent = pt->parent;
 
    if (pt == Tree)
    {
      Tree = leftchild;
    }
    else 
    {
      if (pt == pt->parent->left)
        pt->parent->left = leftchild;
      else
        pt->parent->right = leftchild;
    }
    leftchild->right = pt;
    pt->parent = leftchild;
}

// This function fixes violations caused by BST insertion
void RBTreeFixViolation(Node *pt)
{
    Node *parent_pt = NULL;
    Node *grand_parent_pt = NULL;
 
    while ((pt != Tree) && ((pt->color) != BLACK) &&((pt->parent)->color == RED))
    {
        parent_pt = pt->parent;
        grand_parent_pt = (pt->parent)->parent;
 
        // Case : A - Parent of pt is left child of Grand-parent of pt 
        if (parent_pt == grand_parent_pt->left)
        {
            Node *uncle_pt = grand_parent_pt->right;
            // Case : 1 - The uncle of pt is also red. Only Recoloring required
            if ((uncle_pt != NULL) && (uncle_pt->color == RED))
            {
                grand_parent_pt->color = RED;
                parent_pt->color = BLACK;
                uncle_pt->color = BLACK;
                pt = grand_parent_pt;
            }
            else
            {
                // Case : 2 - pt is right child of its parent. Left-rotation required 
                if (pt == (parent_pt->right))
                {
                    RBTreeRotateLeft(parent_pt);
                    pt = parent_pt;
                    parent_pt = pt->parent;
                }
                Node temp;
                temp.color = parent_pt->color;               
                parent_pt->color = grand_parent_pt->color;
                grand_parent_pt->color = temp.color;
                // Case : 3 - pt is left child of its parent. Right-rotation required 
                RBTreeRotateRight(grand_parent_pt);               
                pt = parent_pt;
            }
        }
        // Case : B - Parent of pt is right child of Grand-parent of pt 
        else
        {
            Node *uncle_pt = grand_parent_pt->left;
            //  Case : 1 - The uncle of pt is also red. Only Recoloring required 
            if ((uncle_pt != NULL) && (uncle_pt->color == RED))
            {
                grand_parent_pt->color = RED;
                parent_pt->color = BLACK;
                uncle_pt->color = BLACK;
                pt = grand_parent_pt;
            }
            else
            {
                // Case : 2 - pt is left child of its parent. Right-rotation required 
                if (pt == parent_pt->left)
                {
                    RBTreeRotateRight(parent_pt);
                    pt = parent_pt;
                    parent_pt = pt->parent;
                }
                // Case : 3 - pt is right child of its parent. Left-rotation required 
                Node temp;
                temp.color = parent_pt->color;               
                parent_pt->color = grand_parent_pt->color;
                grand_parent_pt->color = temp.color;
                RBTreeRotateLeft(grand_parent_pt);                
                pt = parent_pt;
            }
        }
    }
    Tree->color = BLACK;
}

// Function to insert a new node with given data
void RBTreeInsert(TaskInfo data)
{  
     Nodes[i].color = RED;
     Nodes[i].st_node.p_tcb = data.p_tcb;
     Nodes[i].st_node.p_tcb1 = data.p_tcb1;
     Nodes[i].st_node.period = data.period;    
     Nodes[i].st_node.nextReleaseTime = data.nextReleaseTime;     
     Nodes[i].left = NULL;
     Nodes[i].right = NULL;
     Nodes[i].parent = NULL;    
     // Do a normal BST insert
     Tree = BSTInsert(Tree, &Nodes[i]);
 
     // fix Red Black Tree violations
     RBTreeFixViolation(&Nodes[i]);
     i = i+1;
}

void TreeFind(CPU_INT32U currTick) 
{   Node *ptr;
    ptr = RBT_RemoveNode(currTick);
    fibonode* node = Extract_Min();
     if (node!=NULL)
      {
          while ( node->isTail !=1)
          {
            RecRdyListInsert(node->sched_val.p_tcb);
            node=node->next;
          }
         RecRdyListInsert(node->sched_val.p_tcb); 
      }
}

void RecRdyListInsert(OS_TCB* p_tcb)
{
   // --------------- ADD TASK TO READY LIST ---------------    
            OS_PrioInsert(p_tcb->Prio);
            OS_RdyListInsertTail(p_tcb); 
            #if OS_CFG_TRACE_EN > 0u   
            taskScheduleOverhead = ((OS_TS_GET() - ScheduleStart)- (ScheduleStart - StartTime));
            printf("Task Schedule Overhead (Heap Insertion to Ready List Insertion) : %d ticks\n",  taskScheduleOverhead);
            #endif            
            #if OS_CFG_DBG_EN > 0u
            OS_TaskDbgListAdd(p_tcb);
            #endif     
   OSSched();   
}
//------------------RBT Search Node-------------------------
RBTree* RBT_SearchNode(RBTree* tree, CPU_INT32U currTick )
{
   if (tree == NULL)   
      return NULL;   

   if ((tree->st_node).nextReleaseTime != 0)
   {
     if((tree->st_node).nextReleaseTime > currTick)
     {  
        if ( tree->left != NULL)             
           return RBT_SearchNode( tree->left, currTick );        
        else        
          return NULL;        
     }
     else if((tree->st_node).nextReleaseTime < currTick)
     {
         if ( tree->right != NULL)                   
            return RBT_SearchNode( tree->right, currTick );          
         else         
            return NULL;           
     }
    else 
    {
        return tree;
    }
  }
}


//----------------------Search minimum Node-----------------------



RBTree* RBT_SearchMinNode( RBTree* tree )
{
  if( tree == NULL )  
    return NULL;  
  
  while(tree->left != NULL)
     tree = tree->left;
  
  return tree;
}
//----------------------------Find and Remove----------------------
RBTree* RBT_RemoveNode( CPU_INT32U currTick )
{       
    RBTree* tree = Tree;
    Node* removed = NULL;
    Node* successor = NULL;
    Node* target = RBT_SearchNode(tree, currTick);

    if( target == NULL)
        return NULL;	
    
    CPU_INT32U parent_b;
    Color color_b= target->color;
    CPU_INT32U i = findLength(target);
#if OS_CFG_TRACE_EN > 0u 
    StartTime = OS_TS_GET();
    ScheduleStart = OS_TS_GET ();
#endif   
    while(i > 1)
    {
       tree=Tree;
       removed = target; 
      
       target->next->left = target->left;
       target->next->right = target->right;
       target->next->parent = target->parent; 
       target->next->color = color_b;
       target->next->left->parent = target->next;
       target->next->right->parent = target->next;
       
       if (target->parent != NULL)
       {
         if (target == target->parent->left)         
            target->parent->left = target->next;         
         else
           target->parent->right = target->next;
       }  
    
       //target->next->color = target->color;
       if (target == tree)       
          tree = target->next;
       target = target->next;
       target->isHead = 1;
            
       //  if (((removed->st_node).p_tcb->TaskState)==OS_TASK_STATE_RDY)       
       CopyTCB((removed->st_node).p_tcb,&((removed->st_node).p_tcb1)); 
        heap = heap_insert(removed->st_node); 
       (removed->st_node).nextReleaseTime = (OSTickCtr + (removed->st_node).period);          
       removed->left = removed->right = removed->parent = removed->next = NULL;       
       removed->isHead = removed->isTail = 0;
       removed->color = RED;
       tree = BSTInsert(tree, removed);
       Tree = tree;
       RBTreeFixViolation(removed);
       i--;
    }
    tree=Tree;
    if ((target->left == NULL) || (target->right == NULL) )
    {
	removed = target;
    }
    else 
    {
	removed = RBT_SearchMinNode( target->right );
	target->st_node = removed->st_node;
    }

    if( removed->left != NULL )    
	successor = removed->left;    
    else     
	successor = removed->right;
    
    successor->parent = removed->parent;
    if( removed->parent == NULL )
    {
        tree = successor;
    } 
    else 
    {
	if(removed == removed->parent->left)        
            removed->parent->left = successor;	
        else        
            removed->parent->right = successor;		
	
	if( removed->color == BLACK )        
	    RBTreeFixViolation( successor );
    }
    if (removed != NULL)
    {  //  if (((removed->st_node).p_tcb->TaskState)==OS_TASK_STATE_RDY)       
        CopyTCB((removed->st_node).p_tcb,&((removed->st_node).p_tcb1));  
        heap = heap_insert(removed->st_node); 
       (removed->st_node).nextReleaseTime = (OSTickCtr + (removed->st_node).period);    
       // heap = heap_insert(removed->st_node);        
#if OS_CFG_TRACE_EN > 0u         
        CPU_FP32 RBTreeToHeapOverhead = ((OS_TS_GET() - DSInsertStart)- (DSInsertStart - StartTime1));
        printf("Overhead between Red Black Tree Insertion to Schedule Heap Insertion : %d ticks\n",  RBTreeToHeapOverhead);
#endif        
        (removed->st_node).nextReleaseTime = OSTickCtr + ((removed->st_node).period); 
        removed->left = removed->right = removed->parent = removed->next = NULL;
        removed->isHead = removed->isTail = 0;
        removed->color = RED;
        tree = BSTInsert(tree, removed);
        Tree = tree;
        RBTreeFixViolation(removed);
    }    
    return tree;
}
//----------------------------------------Overflow handler Functions-----------------

void overflow_handler()
{   
  if (OSTickCtr& 1073741824u)
  {
      Update_Release_Time(Tree);
      OSTickCtr=0;
  }
}


void Update_Release_Time(RBTree* root)
{    
       RBTree* temp;
       if (root!=NULL)
       {    
            temp=root;
            while(temp->isTail != 1)
               {
                  if (temp->st_node.nextReleaseTime>=OSTickCtr )
                  {
                  temp->st_node.nextReleaseTime=temp->st_node.nextReleaseTime-OSTickCtr;
                  }
                  temp=temp->next;
               }   
              if (temp->st_node.nextReleaseTime>=OSTickCtr)
              {
              temp->st_node.nextReleaseTime=temp->st_node.nextReleaseTime-OSTickCtr;               
              }
       Update_Release_Time(root->left);
       Update_Release_Time(root->right);
      }
}
//-------------------------------Linked List Handlers-----------------------------
//Traverse the list
RBTree* listTraverse(RBTree* tree) 
{
   Node *ptr = tree;
    if(ptr->isTail != 1)   
   {
      
     ptr=listTraverse(ptr->next);
   }
  return ptr;
}

CPU_INT32U findLength(RBTree* tree) {
   CPU_INT32U length = 0;
   Node *current;
   
   for(current = tree; current != NULL; current = current->next)   
      length++;
   
   return length;
}

//------------------------------------------------------------------------------------------------------------------------
//                                        FIBONACCI HEAP
//------------------------------------------------------------------------------------------------------------------------
fibonode* InitializeHeap()
{
    fibonode* np;
    np = NULL;
    heap_count=0;
    return np;
}

fibonode* Create_node(TaskInfo tree_node)
{
    heap_node[j].sched_val.p_tcb = tree_node.p_tcb;
    heap_node[j].sched_val.p_tcb1 = tree_node.p_tcb1;
    heap_node[j].sched_val.period = tree_node.period;
    heap_node[j].sched_val.nextReleaseTime = tree_node.nextReleaseTime;
    heap_node[j].left = heap_node[j].right = &heap_node[j];
    heap_node[j].degree = 0;
    heap_node[j].mark = false;
    heap_node[j].child = NULL;
    heap_node[j].parent = NULL;
    j = j+1;
    return &heap_node[j-1];
}

fibonode* heap_insert(TaskInfo tree_node)
{ 
  fibonode* x;
  x=Create_node(tree_node);
  heap=insert (heap,x);
  return heap;
}

fibonode* insert(fibonode* H, fibonode* x)
{
  fibonode *check= find(H, x->sched_val.period);
    if (check==NULL)
    {
      x->isHead=1;
      x->isTail=1;
      x->next=NULL;
      x->degree = 0;
      x->parent = NULL;
      x->child = NULL;
      x->left = x;
      x->right = x;
      x->mark = 'F';
      x->C = 'N';
      if (H != NULL)
      {  (H->left)->right = x;
        x->right = H;
        x->left = H->left;
        H->left = x;
        if ((x->sched_val).period < (H->sched_val).period)
            H = x;
      }
       else
       {
         H = x;
       }
    }
     else
     { 
       fibonode *ptr;
       ptr = heaplistTraverse(check);
       ptr->next = x;
       ptr->isTail = 0;
       x->isTail = 1;    
       x->isHead = 0; 
       x->left = x->right = x->parent=x->child = NULL;
       x->next = NULL;
      }
      heap_count = heap_count + 1;   //check
    return H;
}

CPU_INT32U Fibonnaci_link(fibonode* H1, fibonode* y, fibonode* z)
{
    (y->left)->right = y->right;
    (y->right)->left = y->left;
    if (z->right == z)
        H1 = z;
    y->left = y;
    y->right = y;
    y->parent = z;
    if (z->child == NULL)
        z->child = y;
    y->right = z->child;
    y->left = (z->child)->left;
    ((z->child)->left)->right = y;
    (z->child)->left = y;
    if ((y->sched_val).period < ((z->child)->sched_val).period)
        z->child = y;
    z->degree++;
}

fibonode* merge(fibonode* H1, fibonode* H2)
{
    fibonode* np;
    fibonode* H = InitializeHeap();
    H = H1;
   (H->left)->right = H2;
   (H2->left)->right = H;
    np = H->left;
    H->left = H2->left;
    H2->left = np;
    return H;
}

fibonode* Extract_Min()
{
    fibonode* p;
    fibonode* ptr;
    fibonode* z = heap;
    p = z;
    ptr = z;
    if (z == NULL)
     return NULL;
      fibonode* x;
    fibonode* np;
    x = NULL;
    if (z->child != NULL)
        x = z->child;
    if (x != NULL)
    {
        ptr = x;
        do
        {
            np = x->right;
            (heap->left)->right = x;
            x->right = heap;
            x->left = heap->left;
            heap->left = x;
            if ((x->sched_val).period < (heap->sched_val).period)
            heap = x;
            x->parent = NULL;
            x = np;
        }
        while (np != ptr);
    }
    (z->left)->right = z->right;
    (z->right)->left = z->left;
    heap = z->right;
    if (z == z->right && z->child == NULL)
    { 
      heap = NULL;
      j=0;
    }
    else
    {
        heap = z->right;
        Consolidate(heap);
    }
   /* if (heap_count==1)
    {
      heap==NULL;
    }*/
    heap_count = heap_count-1;
    return (p);
}

/*

 * Consolidate Node in Fibonnaci Heap

 */

CPU_INT32U Consolidate(fibonode* H1)
{
    CPU_INT32U d, i;
    CPU_FP32 f = (log(heap_count)) / (log(2));
    CPU_INT32U D = (CPU_INT32U)f;
    fibonode* A[32];
    for (i = 0; i <= D; i++)
        A[i] = NULL;
    fibonode* x = H1;
    fibonode* y;
    fibonode* np;
    fibonode* pt = x;
    do
   {
        pt = pt->right;
        d = x->degree;
        while (A[d] != NULL)
        {
            y = A[d];
            if ((x->sched_val).period > (y->sched_val).period)
            {
                np = x;
                x = y;
                y = np;
            }
            if (y == H1)
                H1 = x;
           Fibonnaci_link(H1, y, x);
            if (x->right == x)
                H1 = x;
                A[d] = NULL;
            d = d + 1;
        }
        A[d] = x;
        x = x->right;
    }
    while (x != H1);
    heap = NULL;
    for (int j = 0; j <= D; j++)
    {
        if (A[j] != NULL)
        {
            A[j]->left = A[j];
            A[j]->right =A[j];
            if (heap != NULL)
           {
                (heap->left)->right = A[j];
                A[j]->right = heap;
                A[j]->left = heap->left;
                heap->left = A[j];
                if ((A[j]->sched_val).period < (heap->sched_val).period)
                heap= A[j];
            }
            else
            {
                heap = A[j];
            }
            if(heap == NULL)
                heap= A[j];
            else if ((A[j]->sched_val).period < (heap->sched_val).period)
                heap = A[j];
        }
    }
}


fibonode* find(fibonode* heap, CPU_INT32U period) 
{
    fibonode* n = heap;
    if(n == NULL) return NULL;
    do 
    {
        if((n->sched_val).period == period)return n;
        fibonode* ret = find(n->child, period);
        if(ret)
        return ret;
        n = n->right;
     } while(n != heap);
     return NULL;
}


fibonode* heaplistTraverse(fibonode* heap) 
{
   fibonode *ptr = heap;
   if(ptr->isTail != 1)   
   {      
     ptr=heaplistTraverse(ptr->next);
   }
  return ptr;
}

CPU_INT32U heapfindLength(fibonode* heap) {
   CPU_INT32U length = 0;
   fibonode *current;
   
   for(current = heap; current != NULL; current = current->next)   
      length++;
   
   return length;
}

/*
************************************************************************************************************************
*                                                    CREATE A RECURSIVE TASK
*
* Description: This function is used to have uC/OS-III manage the execution of a task.  Tasks can either be created
*              prior to the start of multitasking or by a running task.  A task cannot be created by an ISR.
*
* Arguments  : p_tcb          is a pointer to the task's TCB
*
*              p_name         is a pointer to an ASCII string to provide a name to the task.
*
*              p_task         is a pointer to the task's code
*
*              p_arg          is a pointer to an optional data area which can be used to pass parameters to
*                             the task when the task first executes.  Where the task is concerned it thinks
*                             it was invoked and passed the argument 'p_arg' as follows:
*
*                                 void Task (void *p_arg)
*                                 {
*                                     for (;;) {
*                                         Task code;
*                                     }
*                                 }
*
*              prio           is the task's priority.  A unique priority MUST be assigned to each task and the
*                             lower the number, the higher the priority.
*
*              p_stk_base     is a pointer to the base address of the stack (i.e. low address).
*
*              stk_limit      is the number of stack elements to set as 'watermark' limit for the stack.  This value
*                             represents the number of CPU_STK entries left before the stack is full.  For example,
*                             specifying 10% of the 'stk_size' value indicates that the stack limit will be reached
*                             when the stack reaches 90% full.
*
*              stk_size       is the size of the stack in number of elements.  If CPU_STK is set to CPU_INT08U,
*                             'stk_size' corresponds to the number of bytes available.  If CPU_STK is set to
*                             CPU_INT16U, 'stk_size' contains the number of 16-bit entries available.  Finally, if
*                             CPU_STK is set to CPU_INT32U, 'stk_size' contains the number of 32-bit entries
*                             available on the stack.
*
*              q_size         is the maximum number of messages that can be sent to the task
*
*              time_quanta    amount of time (in ticks) for time slice when round-robin between tasks.  Specify 0 to use
*                             the default.
*
*              p_ext          is a pointer to a user supplied memory location which is used as a TCB extension.
*                             For example, this user memory can hold the contents of floating-point registers
*                             during a context switch, the time each task takes to execute, the number of times
*                             the task has been switched-in, etc.
*
*              opt            contains additional information (or options) about the behavior of the task.
*                             See OS_OPT_TASK_xxx in OS.H.  Current choices are:
*
*                                 OS_OPT_TASK_NONE            No option selected
*                                 OS_OPT_TASK_STK_CHK         Stack checking to be allowed for the task
*                                 OS_OPT_TASK_STK_CLR         Clear the stack when the task is created
*                                 OS_OPT_TASK_SAVE_FP         If the CPU has floating-point registers, save them
*                                                             during a context switch.
*
*              p_err          is a pointer to an error code that will be set during this call.  The value pointer
*                             to by 'p_err' can be:
*
*                                 OS_ERR_NONE                    if the function was successful.
*                                 OS_ERR_ILLEGAL_CREATE_RUN_TIME if you are trying to create the task after you called
*                                                                   OSSafetyCriticalStart().
*                                 OS_ERR_NAME                    if 'p_name' is a NULL pointer
*                                 OS_ERR_PRIO_INVALID            if the priority you specify is higher that the maximum
*                                                                   allowed (i.e. >= OS_CFG_PRIO_MAX-1) or,
*                                                                if OS_CFG_ISR_POST_DEFERRED_EN is set to 1 and you tried
*                                                                   to use priority 0 which is reserved.
*                                 OS_ERR_STK_INVALID             if you specified a NULL pointer for 'p_stk_base'
*                                 OS_ERR_STK_SIZE_INVALID        if you specified zero for the 'stk_size'
*                                 OS_ERR_STK_LIMIT_INVALID       if you specified a 'stk_limit' greater than or equal
*                                                                   to 'stk_size'
*                                 OS_ERR_TASK_CREATE_ISR         if you tried to create a task from an ISR.
*                                 OS_ERR_TASK_INVALID            if you specified a NULL pointer for 'p_task'
*                                 OS_ERR_TCB_INVALID             if you specified a NULL pointer for 'p_tcb'
*
* Returns    : A pointer to the TCB of the task created.  This pointer must be used as an ID (i.e handle) to the task.
************************************************************************************************************************
*/
/*$PAGE*/
void  OSRecTaskCreate ( OS_TCB        *p_tcb,
                        CPU_CHAR      *p_name,
                        OS_TASK_PTR    p_task,
                        void          *p_arg,
                        OS_PRIO        prio,
                        CPU_STK       *p_stk_base,
                        CPU_STK_SIZE   stk_limit,
                        CPU_STK_SIZE   stk_size,
                        OS_MSG_QTY     q_size,
                        OS_TICK        time_quanta,
                        void          *p_ext,
                        OS_OPT         opt,
                        OS_ERR        *p_err,
                        CPU_INT32U     period)
{
    CPU_STK_SIZE   i;
#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    OS_OBJ_QTY     reg_nbr;
#endif
    CPU_STK       *p_sp;
    CPU_STK       *p_stk_limit;
    CPU_SR_ALLOC();

#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#ifdef OS_SAFETY_CRITICAL_IEC61508
    if (OSSafetyCriticalStartFlag == DEF_TRUE) {
       *p_err = OS_ERR_ILLEGAL_CREATE_RUN_TIME;
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* ---------- CANNOT CREATE A TASK FROM AN ISR ---------- */
        *p_err = OS_ERR_TASK_CREATE_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u                                  /* ---------------- VALIDATE ARGUMENTS ------------------ */
    if (p_tcb == (OS_TCB *)0) {                             /* User must supply a valid OS_TCB                        */
        *p_err = OS_ERR_TCB_INVALID;
        return;
    }
    if (p_task == (OS_TASK_PTR)0) {                         /* User must supply a valid task                          */
        *p_err = OS_ERR_TASK_INVALID;
        return;
    }
    if (p_stk_base == (CPU_STK *)0) {                       /* User must supply a valid stack base address            */
        *p_err = OS_ERR_STK_INVALID;
        return;
    }
    if (stk_size < OSCfg_StkSizeMin) {                      /* User must supply a valid minimum stack size            */
        *p_err = OS_ERR_STK_SIZE_INVALID;
        return;
    }
    if (stk_limit >= stk_size) {                            /* User must supply a valid stack limit                   */
        *p_err = OS_ERR_STK_LIMIT_INVALID;
        return;
    }
    if (prio >= OS_CFG_PRIO_MAX) {                          /* Priority must be within 0 and OS_CFG_PRIO_MAX-1        */
        *p_err = OS_ERR_PRIO_INVALID;
        return;
    }
#endif

#if OS_CFG_ISR_POST_DEFERRED_EN > 0u
    if (prio == (OS_PRIO)0) {
        if (p_tcb != &OSIntQTaskTCB) {
            *p_err = OS_ERR_PRIO_INVALID;                   /* Not allowed to use priority 0                          */
            return;
        }
    }
#endif

    if (prio == (OS_CFG_PRIO_MAX - 1u)) {
        if (p_tcb != &OSIdleTaskTCB) {
            *p_err = OS_ERR_PRIO_INVALID;                   /* Not allowed to use same priority as idle task          */
            return;
        }
    }

    OS_TaskInitTCB(p_tcb);                                  /* Initialize the TCB to default values                   */

    *p_err = OS_ERR_NONE;
                                                            /* --------------- CLEAR THE TASK'S STACK --------------- */
    if ((opt & OS_OPT_TASK_STK_CHK) != (OS_OPT)0) {         /* See if stack checking has been enabled                 */
        if ((opt & OS_OPT_TASK_STK_CLR) != (OS_OPT)0) {     /* See if stack needs to be cleared                       */
            p_sp = p_stk_base;
            for (i = 0u; i < stk_size; i++) {               /* Stack grows from HIGH to LOW memory                    */
                *p_sp = (CPU_STK)0;                         /* Clear from bottom of stack and up!                     */
                p_sp++;
            }
        }
    }
                                                            /* ------- INITIALIZE THE STACK FRAME OF THE TASK ------- */
#if (CPU_CFG_STK_GROWTH == CPU_STK_GROWTH_HI_TO_LO)
    p_stk_limit = p_stk_base + stk_limit;
#else
    p_stk_limit = p_stk_base + (stk_size - 1u) - stk_limit;
#endif

    p_sp = OSTaskStkInit(p_task,
                         p_arg,
                         p_stk_base,
                         p_stk_limit,
                         stk_size,
                         opt);

                                                            /* -------------- INITIALIZE THE TCB FIELDS ------------- */
    p_tcb->TaskEntryAddr = p_task;                          /* Save task entry point address                          */
    p_tcb->TaskEntryArg  = p_arg;                           /* Save task entry argument                               */

    p_tcb->NamePtr       = p_name;                          /* Save task name                                         */

    p_tcb->Prio          = prio;                            /* Save the task's priority                               */
    p_tcb->OriginalPrio          = prio;
    p_tcb->StkPtr        = p_sp;                            /* Save the new top-of-stack pointer                      */
    p_tcb->StkLimitPtr   = p_stk_limit;                     /* Save the stack limit pointer                           */

    p_tcb->TimeQuanta    = time_quanta;                     /* Save the #ticks for time slice (0 means not sliced)    */
#if OS_CFG_SCHED_ROUND_ROBIN_EN > 0u
    if (time_quanta == (OS_TICK)0) {
        p_tcb->TimeQuantaCtr = OSSchedRoundRobinDfltTimeQuanta;
    } else {
        p_tcb->TimeQuantaCtr = time_quanta;
    }
#endif
    p_tcb->ExtPtr        = p_ext;                           /* Save pointer to TCB extension                          */
    p_tcb->StkBasePtr    = p_stk_base;                      /* Save pointer to the base address of the stack          */
    p_tcb->StkSize       = stk_size;                        /* Save the stack size (in number of CPU_STK elements)    */
    p_tcb->Opt           = opt;                             /* Save task options                                      */

#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    for (reg_nbr = 0u; reg_nbr < OS_CFG_TASK_REG_TBL_SIZE; reg_nbr++) {
        p_tcb->RegTbl[reg_nbr] = (OS_REG)0;
    }
#endif

#if OS_CFG_TASK_Q_EN > 0u
    OS_MsgQInit(&p_tcb->MsgQ,                               /* Initialize the task's message queue                    */
                q_size);
#endif        
    OSTaskCreateHook(p_tcb);                                /* Call user defined hook                                 */
    //CPU_INT32U nextReleaseTime = OSTickCtr + period;
    OS_TCB p_tcb1 = *(p_tcb);
    TaskInfo x = {.p_tcb = p_tcb, .p_tcb1 = p_tcb1, .period = period, .nextReleaseTime = OSTickCtr+5};        
#if OS_CFG_TRACE_EN > 0u     
    StartTime1 = OS_TS_GET();
    DSInsertStart = OS_TS_GET ();
#endif    
    RBTreeInsert(x);    
    OSTaskQty++;    
}

/*$PAGE*/
/*
************************************************************************************************************************
*                                      DELETE A RECURSIVE TASK
*
* Description: This function allows you to delete a task.  The calling task can delete itself by specifying a NULL
*              pointer for 'p_tcb'.  The deleted task is returned to the dormant state and can be re-activated by
*              creating the deleted task again.
*
* Arguments  : p_tcb      is the TCB of the tack to delete
*
*              p_err      is a pointer to an error code returned by this function:
*
*                             OS_ERR_NONE                  if the call is successful
*                             OS_ERR_STATE_INVALID         if the state of the task is invalid
*                             OS_ERR_TASK_DEL_IDLE         if you attempted to delete uC/OS-III's idle task
*                             OS_ERR_TASK_DEL_INVALID      if you attempted to delete uC/OS-III's ISR handler task
*                             OS_ERR_TASK_DEL_ISR          if you tried to delete a task from an ISR
************************************************************************************************************************
*/

#if OS_CFG_TASK_DEL_EN > 0u
void  OSRecTaskDel (OS_TCB  *p_tcb,
                    OS_ERR  *p_err)
{
    CPU_SR_ALLOC();



#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* See if trying to delete from ISR                       */
       *p_err = OS_ERR_TASK_DEL_ISR;
        return;
    }
#endif

    if (p_tcb == &OSIdleTaskTCB) {                          /* Not allowed to delete the idle task                    */
        *p_err = OS_ERR_TASK_DEL_IDLE;
        return;
    }

#if OS_CFG_ISR_POST_DEFERRED_EN > 0u
    if (p_tcb == &OSIntQTaskTCB) {                          /* Cannot delete the ISR handler task                     */
        *p_err = OS_ERR_TASK_DEL_INVALID;
        return;
    }
#endif

    if (p_tcb == (OS_TCB *)0) {                             /* Delete 'Self'?                                         */
        CPU_CRITICAL_ENTER();
        p_tcb  = OSTCBCurPtr;                               /* Yes.                                                   */
        CPU_CRITICAL_EXIT();
    }

    OS_CRITICAL_ENTER();
    switch (p_tcb->TaskState) {
        case OS_TASK_STATE_RDY:
             OS_RdyListRemove(p_tcb);
             break;

        case OS_TASK_STATE_SUSPENDED:
             break;

        case OS_TASK_STATE_DLY:                             /* Task is only delayed, not on any wait list             */
        case OS_TASK_STATE_DLY_SUSPENDED:
             OS_TickListRemove(p_tcb);
             break;

        case OS_TASK_STATE_PEND:
        case OS_TASK_STATE_PEND_SUSPENDED:
        case OS_TASK_STATE_PEND_TIMEOUT:
        case OS_TASK_STATE_PEND_TIMEOUT_SUSPENDED:
             OS_TickListRemove(p_tcb);
             switch (p_tcb->PendOn) {                       /* See what we are pending on                             */
                 case OS_TASK_PEND_ON_NOTHING:
                 case OS_TASK_PEND_ON_TASK_Q:               /* There is no wait list for these two                    */
                 case OS_TASK_PEND_ON_TASK_SEM:
                      break;

                 case OS_TASK_PEND_ON_FLAG:                 /* Remove from wait list                                  */
                 case OS_TASK_PEND_ON_MULTI:
                 case OS_TASK_PEND_ON_MUTEX:
                 case OS_TASK_PEND_ON_Q:
                 case OS_TASK_PEND_ON_SEM:
                      OS_PendListRemove(p_tcb);
                      break;

                 default:
                      break;
             }
             break;

        default:
            OS_CRITICAL_EXIT();
            *p_err = OS_ERR_STATE_INVALID;
            return;
    }

#if OS_CFG_TASK_Q_EN > 0u
    (void)OS_MsgQFreeAll(&p_tcb->MsgQ);                     /* Free task's message queue messages                     */
#endif
    //OSTaskDelHook(p_tcb);                                   /* Call user defined hook                                 */
    CPU_STK *p_sp = p_tcb->StkBasePtr;
    CPU_INT32U i;
    for (i = 0u; i < p_tcb->StkSize; i++) {               /* Stack grows from HIGH to LOW memory                    */
        *p_sp = (CPU_STK)0;                         /* Clear from bottom of stack and up!                     */
         p_sp++;
     }
#if OS_CFG_DBG_EN > 0u
    OS_TaskDbgListRemove(p_tcb);
#endif
    OS_CRITICAL_EXIT_NO_SCHED();
    OSSched();                                              /* Find new highest priority task                         */
    *p_err = OS_ERR_NONE;
}
#endif

/*$PAGE*/
/*
************************************************************************************************************************
*                                             COPY TCB VALUE
*
* Description: This function allows you to copy task TCB from a copy
*
* Arguments  : p_tcb      is the TCB of the tack to be executed
*
*              tcb        is a a TCB copy of the task
*
************************************************************************************************************************
*/
void CopyTCB(OS_TCB *p_tcb, OS_TCB *tcb)
{
  CPU_STK       *p_sp;
  CPU_STK       *p_stk_limit;
/* ------- INITIALIZE THE STACK FRAME OF THE TASK ------- */
  
#if (CPU_CFG_STK_GROWTH == CPU_STK_GROWTH_HI_TO_LO)
    p_stk_limit = tcb->StkBasePtr + *(tcb->StkLimitPtr);
#else
    p_stk_limit = tcb->StkBasePtr + (tcb->StkSize - 1u) - tcb->StkLimitPtr;
#endif

    p_sp = OSTaskStkInit(tcb->TaskEntryAddr,
                         tcb->TaskEntryArg,
                         tcb->StkBasePtr,
                         tcb->StkBasePtr,
                         tcb->StkSize,
                         tcb->Opt);

                                                            /* -------------- INITIALIZE THE TCB FIELDS ------------- */
    p_tcb->TaskEntryAddr = tcb->TaskEntryAddr;                          /* Save task entry point address                          */
    p_tcb->TaskEntryArg  = tcb->TaskEntryArg;                           /* Save task entry argument                               */

    p_tcb->NamePtr       = tcb->NamePtr;                          /* Save task name                                         */

    p_tcb->Prio          = tcb->Prio;                            /* Save the task's priority                               */
    p_tcb->OriginalPrio          = tcb->OriginalPrio;
    p_tcb->StkPtr        = tcb->StkPtr;                            /* Save the new top-of-stack pointer                      */
    p_tcb->StkLimitPtr   = tcb->StkLimitPtr;     
    
    p_tcb->TimeQuanta    = tcb->TimeQuanta;                     /* Save the #ticks for time slice (0 means not sliced)    */
#if OS_CFG_SCHED_ROUND_ROBIN_EN > 0u
    if (time_quanta == (OS_TICK)0) 
    {
        p_tcb->TimeQuantaCtr = OSSchedRoundRobinDfltTimeQuanta;
    } else 
    {
        p_tcb->TimeQuantaCtr = time_quanta;
    }
#endif
    p_tcb->ExtPtr        = tcb->ExtPtr;                           /* Save pointer to TCB extension                          */
    p_tcb->StkBasePtr    = tcb->StkBasePtr;                      /* Save pointer to the base address of the stack          */
    p_tcb->StkSize       = tcb->StkSize;                        /* Save the stack size (in number of CPU_STK elements)    */
    p_tcb->Opt           = tcb->Opt;                             /* Save task options                                      */

#if OS_CFG_TASK_REG_TBL_SIZE > 0u
    for (reg_nbr = 0u; reg_nbr < OS_CFG_TASK_REG_TBL_SIZE; reg_nbr++) {
        p_tcb->RegTbl[reg_nbr] = (OS_REG)0;
    }
#endif

#if OS_CFG_TASK_Q_EN > 0u
    OS_MsgQInit(&p_tcb->MsgQ,                               /* Initialize the task's message queue                    */
                tcb->MsgSize);
#endif

    OSTaskCreateHook(p_tcb);                                /* Call user defined hook                                 */    
}