#ifndef OS_RESOURCEACCESS_C
    #define OS_RESOURCEACCESS_C
#endif

#include <os.h>
//------------Typedefs-------------
typedef struct avl_node
{
    OS_PCP_MUTEX *p_mutex;
    OS_TCB *p_tcb;
    struct avl_node *left,*right, *next;
    CPU_INT32U ht;
    CPU_INT32U isHead;
    CPU_INT32U isTail;
}avl_node;

CPU_INT32U System_ceil=1024u;

typedef struct splay_node{
	OS_PCP_MUTEX *p_mutex;
        OS_TCB *p_tcb;
	struct splay_node* lchild;
	struct splay_node* rchild;
}splay_node;

splay_node *splayRoot = NULL;
avl_node *avlRoot = NULL;
//---------Function Prototypes------
avl_node* avlInsert(avl_node *T, OS_PCP_MUTEX *p_mutex, OS_TCB *p_tcb);
avl_node* avlSearch(CPU_INT32U key);
avl_node* avlListTraverse(avl_node* node);
avl_node* avlRotateRight(avl_node *);
avl_node* avlRotateLeft(avl_node *);
avl_node* InsertinList (avl_node* ptr, avl_node* x);
avl_node* RR(avl_node *);
avl_node* LL(avl_node *);
avl_node* LR(avl_node *);
avl_node* RL(avl_node *);
CPU_INT32U BF(avl_node *);
CPU_INT32U height( avl_node *);
avl_node* avlFind(avl_node *T,CPU_INT32U key);
CPU_INT32U avl_Search_mutex(OS_PCP_MUTEX* p_mutex, avl_node* root);

inline splay_node* splay_RR_Rotate(splay_node* k2);
inline splay_node* splay_LL_Rotate(splay_node* k2);
splay_node* Splay(CPU_INT32U key, splay_node* root);
CPU_INT32U splay_Search_tcb(OS_TCB* p_tcb, CPU_INT32U key,splay_node* root);
splay_node* splay_SearchMinNode( splay_node* splay_node );
splay_node* splay_New_Node(OS_PCP_MUTEX *p_mutex, OS_TCB* p_tcb);
splay_node* splay_Insert(OS_PCP_MUTEX *p_mutex, OS_TCB *p_tcb, splay_node* root);
splay_node* splay_Delete(CPU_INT32U RCeil, splay_node* root);
splay_node* splay_Search(CPU_INT32U RCeil, splay_node* root);
CPU_INT32U Calculate_New_Prio(OS_TCB* p_tcb,splay_node* root);
void Preorder(OS_TCB* p_tcb,splay_node* root,CPU_INT32U *maxprio);
void Update_Prio(splay_node* root,CPU_INT32U RCeil,CPU_INT32U key,CPU_INT32U Prio);
//---------------------Mutex Related Functions-------------------------------
void  OSPCPMutexCreate               (OS_PCP_MUTEX    *p_mutex,
                                      CPU_INT32U       RCeil,
                                      CPU_INT32U        p_name,
                                      OS_ERR          *p_err);

void  OSPCPMutexPend                  (OS_PCP_MUTEX   *p_mutex,
                                       OS_TICK     timeout,
                                       CPU_INT32U    *flag,
                                       OS_OPT      opt,
                                       CPU_TS     *p_ts,
                                       OS_ERR     *p_err);

void    OSPCPMutexPost                (OS_PCP_MUTEX          *p_mutex,
                                       OS_OPT                 opt,
                                       OS_ERR                *p_err);
//----------------------Memory block for avl node-----------------------------
OS_MEM avlMemBlock;
CPU_INT32U avlBlock[10][20];

OS_MEM splayMemBlock;
CPU_INT32U splayBlock[10][20];

OS_ERR err;
//---------------------------AVL Tree Utility Functions-----------------------
void avl_create()
{
  OSMemCreate((OS_MEM *) &avlMemBlock, "AVL Memory block init", &avlBlock[0][0], (OS_MEM_QTY)10, (OS_MEM_SIZE)(20*sizeof(CPU_INT32U)), &err);
}

avl_node* avlListTraverse(avl_node* node) 
{
   avl_node *ptr = node;
   while(ptr->isTail != 1)   
   {      
      ptr = ptr->next;
   }
   return ptr;
}

avl_node *avlInsert(avl_node *T, OS_PCP_MUTEX *p_mutex, OS_TCB *p_tcb)
{
    avl_node *check = avlFind(T, p_mutex->ID);
    if (check == NULL)
    {
       if (T==NULL)
       {
          T=(avl_node*)OSMemGet(&avlMemBlock, &err);        
          T->p_mutex = p_mutex;
          T->p_tcb = p_tcb;
          T->left=T->right=T->next=NULL;
          T->isHead=T->isTail=1;             
       }
        
       if(p_mutex->ID > T->p_mutex->ID)        // insert in right subtree
       {
          T->right=avlInsert(T->right, p_mutex, p_tcb);
          if(BF(T)==-2)
            if((p_mutex->ID) > (T->right->p_mutex->ID))
                T=RR(T);
            else
                T=RL(T);
       }
       else if((p_mutex->ID) < T->p_mutex->ID)
       {
           T->left=avlInsert(T->left, p_mutex, p_tcb);
           if(BF(T)==2)
              if((p_mutex->ID) < T->left->p_mutex->ID)
                  T=LL(T);
              else
                  T=LR(T);
        }      
        T->ht=height(T);
    }
    else
    {
       avl_node *x;
       x = (avl_node*)OSMemGet(&avlMemBlock, &err);
       x->p_mutex = p_mutex;
       x->p_tcb = p_tcb;
       x->left = x->right = x->next = NULL;
       check=InsertinList (check,x);               
    }
    return(T);
}

avl_node* InsertinList (avl_node* ptr, avl_node* x)
{
    avl_node* current=ptr;
    if ((current->isHead==1) && ((current->p_tcb->Prio)<(x->p_tcb->Prio)))
    {
       x->next=current;
       x->isHead=1;
       x->isTail=0;
       x->left=current->left;
       x->right=current->right;  
       x->ht=current->ht;
       current->isHead=0;
       current->isTail=1;
       current->left=current->right=NULL;
       current=x;
     }
     else
     {
         current = ptr;
         while((current->isTail!=1)&& current->next->p_tcb->Prio > x->p_tcb->Prio)
         {
            current = current->next;
         }
         if ((current->isTail==1) &&  ( current->p_tcb->Prio > x->p_tcb->Prio))
         {
            x->next = NULL;
            x->isTail=1;
            x->isHead=0;
            current->isTail=0;
            current->next=x;              
         }
         else
         {
            x->next = current->next;
            x->isTail=0;
            x->isHead=0;
            current->next = x;
         }
      } 
      return ptr;
}

CPU_INT32U max(CPU_INT32U a,CPU_INT32U b)
{
    return a > b ? a : b;
}

CPU_INT32U avl_height(avl_node* p)
{
    return p ? p->ht : 0;
}

void recalc(avl_node* p)
{
    p -> ht= 1 + max(avl_height(p -> left), avl_height(p -> right));
}

avl_node* balance(avl_node* p)
{
    recalc(p);
    if ( avl_height(p -> left) - avl_height(p -> right) == 2 )
    {
        if ( avl_height(p -> left -> right) > avl_height(p -> left -> left) )
            p -> left = avlRotateLeft(p -> left);
        return avlRotateRight(p);
    }
    else if ( avl_height(p -> right) - avl_height(p -> left) == 2 )
    {
        if ( avl_height(p -> right -> left) > avl_height(p -> right -> right) )
            p -> right = avlRotateRight(p -> right);
        return avlRotateLeft(p);
    }
    return p;
}

avl_node* find_min(avl_node* p)
{
    if ( p -> left != NULL )
        return find_min(p -> left);
    else
        return p;
}

avl_node* remove_min(avl_node* p)
{
    if ( p -> left == NULL )
        return p -> right;
    p -> left = remove_min(p -> left);
    return balance(p);
}

avl_node* remove_item(avl_node* p, int key)
{
    if ( !p )
        return NULL;

    if ( key < p -> p_mutex->ID )
        p -> left = remove_item(p -> left, key);
    else if ( key > p -> p_mutex->ID )
        p -> right = remove_item(p -> right, key);
    else
    {
        avl_node* l = p -> left;
        avl_node* r = p -> right;
        OSMemPut(&avlMemBlock,(void*) p,&err);

        if ( r == NULL )
            return l;

         avl_node* m = find_min(r);
         m -> right = remove_min(r);  
         m -> left = l;		  
         return balance(m);
    }
    return balance(p);
}

CPU_INT32U height(avl_node *T)
{
    CPU_INT32U lh,rh;
    if(T==NULL)
        return(0);
    
    if(T->left==NULL)
        lh=0;
    else
        lh=1+T->left->ht;
        
    if(T->right==NULL)
        rh=0;
    else
        rh=1+T->right->ht;
    
    if(lh>rh)
        return(lh);
    
    return(rh);
}
 
avl_node * avlRotateRight(avl_node *x)
{
    avl_node *y;
    y=x->left;
    x->left=y->right;
    y->right=x;
    x->ht=height(x);
    y->ht=height(y);
    return(y);
}
 
avl_node * avlRotateLeft(avl_node *x)
{
    avl_node *y;
    y=x->right;
    x->right=y->left;
    y->left=x;
    x->ht=height(x);
    y->ht=height(y);    
    return(y);
}
 
avl_node * RR(avl_node *T)
{
    T=avlRotateLeft(T);
    return(T);
}
 
avl_node * LL(avl_node *T)
{
    T=avlRotateRight(T);
    return(T);
}
 
avl_node * LR(avl_node *T)
{
    T->left=avlRotateLeft(T->left);
    T=avlRotateRight(T);    
    return(T);
}
 
avl_node * RL(avl_node *T)
{
    T->right=avlRotateRight(T->right);
    T=avlRotateLeft(T);
    return(T);
}
 
CPU_INT32U BF(avl_node *T)
{
    CPU_INT32U lh,rh;
    if(T==NULL)
        return(0);
 
    if(T->left==NULL)
        lh=0;
    else
        lh=1+T->left->ht;
 
    if(T->right==NULL)
        rh=0;
    else
        rh=1+T->right->ht;
 
    return(lh-rh);
}
 
avl_node* avlFind(avl_node *T,CPU_INT32U key)
{
    if(T==NULL)      
      return NULL;
        
    if(T->p_mutex->ID < key)       
    {
      if ( T->right != NULL)    
        return avlFind(T->right,key);                
    }
     else if(T->p_mutex->ID > key)       
     {
       if(T->left != NULL)      
         return avlFind(T->left,key);       
     }
     else 
         return T;          
}
 
CPU_INT32U avl_Search_mutex(OS_PCP_MUTEX* p_mutex, avl_node* root)
{
    if(root==NULL)      
      return 0;
      
    avl_node* left_tree=root;
    avl_node* right_tree=root;
    while ((left_tree!=NULL)||(right_tree!=NULL))
    {
       if ((left_tree->p_mutex==p_mutex)||(right_tree->p_mutex==p_mutex ))
       {
         return 1;
       }
       else
       { 
         avl_Search_mutex(p_mutex,(left_tree->left))  ;
         avl_Search_mutex(p_mutex,(right_tree->right));
         return 0;
       }
    }      
}
//---------------------------Splay Tree Utility Functions-----------------------
void splay_create()
{
  OSMemCreate((OS_MEM *) &splayMemBlock, "Splay Memory block init", &splayBlock[0][0], (OS_MEM_QTY)10, (OS_MEM_SIZE)(20*sizeof(CPU_INT32U)), &err);
}

inline splay_node* splay_RR_Rotate(splay_node* k2)
{
	splay_node* k1 = k2->lchild;
	k2->lchild = k1->rchild;
	k1->rchild = k2;
	return k1;
}

inline splay_node* splay_LL_Rotate(splay_node* k2)
{
	splay_node* k1 = k2->rchild;
	k2->rchild = k1->lchild;
	k1->lchild = k2;
	return k1;
}

/* An implementation of top-down splay tree 
 If key is in the tree, then the node containing the key will be rotated to root,
 else the last non-NULL node (on the search path) will be rotated to root.
 */
splay_node* Splay(CPU_INT32U key, splay_node* root) 
{
	if(!root)
		return NULL;
	splay_node header;
	/* header.rchild points to L tree; header.lchild points to R Tree */
	header.lchild = header.rchild = NULL;
	splay_node* LeftTreeMax = &header;
	splay_node* RightTreeMin = &header;
	
	/* loop until root->lchild == NULL || root->rchild == NULL; then break!
	   (or when find the key, break too.)
	 The zig/zag mode would only happen when cannot find key and will reach
	 null on one side after RR or LL Rotation.
	 */
	while(1)
	{
           if(key < root->p_mutex->ID)
	   {
		if(!root->lchild)
		    break;
		if(key < root->lchild->p_mutex->ID)
		{
	            root = splay_RR_Rotate(root); /* only zig-zig mode need to rotate once,
						   because zig-zag mode is handled as zig
						   mode, which doesn't require rotate, 
						   just linking it to R Tree */
		    if(!root->lchild)
			break;
		}
		/* Link to R Tree */
		RightTreeMin->lchild = root;
		RightTreeMin = RightTreeMin->lchild;
		root = root->lchild;
		RightTreeMin->lchild = NULL;
	    }
	    else if(key > root->p_mutex->ID)
	    {
		if(!root->rchild)
			break;
		if(key > root->rchild->p_mutex->ID)
		{
		    root = splay_LL_Rotate(root);/* only zag-zag mode need to rotate once,
						  because zag-zig mode is handled as zag
						  mode, which doesn't require rotate, 
						  just linking it to L Tree */
		    if(!root->rchild)
			break;
		}
		/* Link to L Tree */
		LeftTreeMax->rchild = root;
		LeftTreeMax = LeftTreeMax->rchild;
		root = root->rchild;
		LeftTreeMax->rchild = NULL;
	    }
	    else
		break;
	}
	/* assemble L Tree, Middle Tree and R tree together */
	LeftTreeMax->rchild = root->lchild;
	RightTreeMin->lchild = root->rchild;
	root->lchild = header.rchild;
	root->rchild = header.lchild;	
	return root;
}

splay_node* splay_search_mutex(CPU_INT32U key, splay_node* root) 
{
	if(!root)
		return NULL;
	splay_node header;
	/* header.rchild points to L tree; header.lchild points to R Tree */
	header.lchild = header.rchild = NULL;
	splay_node* LeftTreeMax = &header;
	splay_node* RightTreeMin = &header;
	
	/* loop until root->lchild == NULL || root->rchild == NULL; then break!
	   (or when find the key, break too.)
	 The zig/zag mode would only happen when cannot find key and will reach
	 null on one side after RR or LL Rotation.
	 */
	while(1)
	{
           if(key < root->p_mutex->RCeil)
	   {
		if(!root->lchild)
			break;
		if(key < root->lchild->p_mutex->RCeil)
		{
	            root = splay_RR_Rotate(root); /* only zig-zig mode need to rotate once,
						   because zig-zag mode is handled as zig
						   mode, which doesn't require rotate, 
						   just linking it to R Tree */
		    if(!root->lchild)
			break;
		}
		/* Link to R Tree */
		RightTreeMin->lchild = root;
		RightTreeMin = RightTreeMin->lchild;
		root = root->lchild;
		RightTreeMin->lchild = NULL;
	   }
	   else if(key > root->p_mutex->RCeil)
	   {
		if(!root->rchild)
			break;
		if(key > root->rchild->p_mutex->RCeil)
		{
	             root = splay_LL_Rotate(root);/* only zag-zag mode need to rotate once,
						  because zag-zig mode is handled as zag
						  mode, which doesn't require rotate, 
						  just linking it to L Tree */
		     if(!root->rchild)
			  break;
		}
		/* Link to L Tree */
		LeftTreeMax->rchild = root;
		LeftTreeMax = LeftTreeMax->rchild;
		root = root->rchild;
		LeftTreeMax->rchild = NULL;
	    }
	    else
		break;
	}
	/* assemble L Tree, Middle Tree and R tree together */
	LeftTreeMax->rchild = root->lchild;
	RightTreeMin->lchild = root->rchild;
	root->lchild = header.rchild;
	root->rchild = header.lchild;	
	return root;
}

splay_node* splay_SearchMinNode( splay_node* splay_node )
{
    if( splay_node == NULL )  
      return NULL;  
    
    while(splay_node->lchild != NULL)
       splay_node = splay_node->lchild;
    
    return splay_node;
}
splay_node* splay_New_Node(OS_PCP_MUTEX *p_mutex, OS_TCB* p_tcb)
{
    splay_node* p_node = (splay_node*)OSMemGet(&splayMemBlock, &err);	
    p_node->p_mutex = p_mutex;
    p_node->p_tcb = p_tcb;
    p_node->lchild = p_node->rchild = NULL;
    return p_node;
}

/* Implementation 1: 
   First Splay(key, root)(and assume the tree we get is called *), so root node and 
   its left child tree will contain nodes with keys <= key, so we could rebuild 
   the tree, using the newly alloced node as a root, the children of original tree
   *(including root node of *) as this new node's children.

NOTE: This implementation is much better! Reasons are as follows in implementation 2.
NOTE: This implementation of splay tree doesn't allow nodes of duplicate keys!
 */
splay_node* splay_Insert(OS_PCP_MUTEX *p_mutex, OS_TCB *p_tcb, splay_node* root)
{
	static splay_node* p_node = NULL;
        splay_node* sp;
        sp=splay_SearchMinNode(root);
        if ((sp->p_mutex->RCeil)>(p_mutex->RCeil))
        {
          System_ceil=(p_mutex->RCeil);
        }

	if(!p_node)
		p_node = splay_New_Node(p_mutex, p_tcb);
	else 
        { // could take advantage of the node remains because of there was duplicate key before.
		p_node->p_mutex = p_mutex;
                p_node->p_tcb = p_tcb;
        }
	if(!root)
	{
		root = p_node;
		p_node = NULL;
		return root;
	}
	root = Splay(p_mutex->ID, root); 
	/* This is BST that, all keys <= root->key is in root->lchild, all keys > 
	   root->key is in root->rchild. (This BST doesn't allow duplicate keys.) */
	if((p_mutex->ID) < (root->p_mutex->ID))
	{
		p_node->lchild = root->lchild;
		p_node->rchild = root;
		root->lchild = NULL;
		root = p_node;
	}
	else if((p_mutex->ID) > (root->p_mutex->ID))
	{
		p_node->rchild = root->rchild;
		p_node->lchild = root;
		root->rchild = NULL;
		root = p_node;
	}
	else
		return root;
	p_node = NULL;
	return root;
}

splay_node* splay_Delete(CPU_INT32U ID, splay_node* root)
{
	splay_node* temp;
	if(!root)
		return NULL;
	root = Splay(ID, root); 
	if(ID != (root->p_mutex->ID)) // No such node in splay tree
		return root;
	else
	{
		if(!root->lchild)
		{
		    temp = root;
		    root = root->rchild;
		}
		else
		{
		    temp = root;
		/*Note: Since key == root->key, so after Splay(key, root->lchild), 
		  the tree we get will have no right child tree. (key > any key in 
		  root->lchild)*/
		     root = Splay(ID, root->lchild); 
		     root->rchild = temp->rchild;
		}
		OSMemPut(&splayMemBlock,(void*) temp,&err);
		return root;
	}
}

void Update_Prio(splay_node* root,CPU_INT32U RCeil,CPU_INT32U key,CPU_INT32U Prio)
{    
     if (root!=NULL)
     {
        if (root->p_mutex->RCeil==RCeil)
         {
          root->p_tcb->Prio= Prio;
         }
         if (root->p_mutex->ID <key)
         {
            Update_Prio(root->lchild,RCeil,key,Prio);
         }
         else if (root->p_mutex->ID >key)
         {
            Update_Prio(root->rchild,RCeil,key,Prio);
         }
     }
}

splay_node* splay_Search(CPU_INT32U ID, splay_node* root)
{
    return Splay(ID, root);
}

CPU_INT32U splay_Search_tcb(OS_TCB* p_tcb, CPU_INT32U key,splay_node* root)
{
      if(root==NULL)      
      return 0;
      
      splay_node* left_tree=root;
      splay_node* right_tree=root;
      while ((left_tree!=NULL)||(right_tree!=NULL))
      {
         if (((left_tree->p_tcb==p_tcb)&&(left_tree->p_mutex->ID!=key))||((right_tree->p_tcb==p_tcb )&&(right_tree->p_mutex->ID!=key)))
         {
           return 1;
         }
         else
         { 
            splay_Search_tcb(p_tcb,key,(left_tree->lchild))  ;
            splay_Search_tcb(p_tcb,key,(right_tree->rchild));
            return 0;
         }
      }      
}

CPU_INT32U Calculate_New_Prio(OS_TCB* p_tcb,splay_node* root)
{
      CPU_INT32U maxprio=256u;
      Preorder(p_tcb,root,&maxprio);
      return maxprio;
}

void Preorder(OS_TCB* p_tcb,splay_node* root,CPU_INT32U *maxprio)
{
   avl_node* sp=NULL;
   if (root!=NULL)
   {
      if (root->p_tcb==p_tcb)
      {
        sp= avlFind(avlRoot,(root->p_mutex->ID));
        if (sp!=NULL)
        { 
           while(sp->isTail != 1)
           {
              if (sp->p_tcb->Prio<*maxprio)
              {
                 *maxprio=sp->p_tcb->Prio;
              }
              sp=sp->next;
           }
           if (sp->p_tcb->Prio<*maxprio)
           {
               *maxprio=sp->p_tcb->Prio;
           }
        }   
     }
     Preorder(p_tcb,root->lchild,maxprio);
     Preorder(p_tcb,root->rchild,maxprio);
   }
}
//---------------PCP Resource Access Protocol------------
void  OSPCPMutexCreate (OS_PCP_MUTEX    *p_mutex,
                        CPU_INT32U       RCeil,
                        CPU_INT32U        p_name,
                        OS_ERR          *p_err)
{
    CPU_SR_ALLOC();
#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#ifdef OS_SAFETY_CRITICAL_IEC61508
    if (OSSafetyCriticalStartFlag == DEF_TRUE) {
       *p_err = OS_ERR_ILLEGAL_CREATE_RUN_TIME;
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* Not allowed to be called from an ISR                   */
        *p_err = OS_ERR_CREATE_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_PCP_MUTEX *)0) {                         /* Validate 'p_mutex'                                     */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return;
    }
#endif
    CPU_CRITICAL_ENTER();
    p_mutex->Type              =  OS_OBJ_TYPE_MUTEX;        /* Mark the data structure as a mutex                     */
    p_mutex->ID                =  p_name;
    p_mutex->OwnerTCBPtr       = (OS_TCB       *)0;
    p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)0;         /* Mutex is available                                     */
    p_mutex->TS                = (CPU_TS        )0;
    p_mutex->OwnerOriginalPrio =  OS_CFG_PRIO_MAX;
    p_mutex->RCeil             =  RCeil;
    OS_PendListInit(&p_mutex->PendList);                    /* Initialize the waiting list                            */
    OSMutexQty++;
    CPU_CRITICAL_EXIT();
    *p_err = OS_ERR_NONE;
}

/*$PAGE*/
/*
************************************************************************************************************************
*                                                    PEND ON MUTEX (PCP PROTOCOL)
* Description: This function waits for a mutex.
* Arguments  : p_mutex       is a pointer to the mutex
*              timeout       is an optional timeout period (in clock ticks).  If non-zero, your task will wait for the
*                            resource up to the amount of time (in 'ticks') specified by this argument.  If you specify
*                            0, however, your task will wait forever at the specified mutex or, until the resource
*                            becomes available.
*              opt           determines whether the user wants to block if the mutex is not available or not:
*                                OS_OPT_PEND_BLOCKING
*                                OS_OPT_PEND_NON_BLOCKING
*              p_ts          is a pointer to a variable that will receive the timestamp of when the mutex was posted or
*                            pend aborted or the mutex deleted.  If you pass a NULL pointer (i.e. (CPU_TS *)0) then you
*                            will not get the timestamp.  In other words, passing a NULL pointer is valid and indicates
*                            that you don't need the timestamp.
*              p_err         is a pointer to a variable that will contain an error code returned by this function.
*                                OS_ERR_NONE               The call was successful and your task owns the resource
*                                OS_ERR_MUTEX_OWNER        If calling task already owns the mutex
*                                OS_ERR_OBJ_DEL            If 'p_mutex' was deleted
*                                OS_ERR_OBJ_PTR_NULL       If 'p_mutex' is a NULL pointer.
*                                OS_ERR_OBJ_TYPE           If 'p_mutex' is not pointing at a mutex
*                                OS_ERR_OPT_INVALID        If you didn't specify a valid option
*                                OS_ERR_PEND_ABORT         If the pend was aborted by another task
*                                OS_ERR_PEND_ISR           If you called this function from an ISR and the result
*                                                          would lead to a suspension.
*                                OS_ERR_PEND_WOULD_BLOCK   If you specified non-blocking but the mutex was not
*                                                          available.
*                                OS_ERR_SCHED_LOCKED       If you called this function when the scheduler is locked
*                                OS_ERR_STATE_INVALID      If the task is in an invalid state
*                                OS_ERR_STATUS_INVALID     If the pend status has an invalid value
*                                OS_ERR_TIMEOUT            The mutex was not received within the specified timeout.
* Returns    : none
************************************************************************************************************************
*/
void  OSPCPMutexPend (OS_PCP_MUTEX   *p_mutex,
                      OS_TICK     timeout,
                      CPU_INT32U    *flag,
                      OS_OPT      opt,
                      CPU_TS     *p_ts,
                      OS_ERR     *p_err)
{
    OS_PEND_DATA  pend_data;
    OS_TCB       *p_tcb;
    CPU_SR_ALLOC();

#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* Not allowed to call from an ISR                        */
       *p_err = OS_ERR_PEND_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_PCP_MUTEX *)0) {                         /* Validate arguments                                     */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return;
    }
    switch (opt) {
        case OS_OPT_PEND_BLOCKING:
        case OS_OPT_PEND_NON_BLOCKING:
             break;

        default:
             *p_err = OS_ERR_OPT_INVALID;
             return;
    }
#endif

#if OS_CFG_OBJ_TYPE_CHK_EN > 0u
    if (p_mutex->Type != OS_OBJ_TYPE_MUTEX) {               /* Make sure mutex was created                            */
        *p_err = OS_ERR_OBJ_TYPE;
        return;
    }
#endif

    if (p_ts != (CPU_TS *)0) {
       *p_ts  = (CPU_TS  )0;                                /* Initialize the returned timestamp                      */
    }

    CPU_CRITICAL_ENTER();
    if (((OSTCBCurPtr->Prio)<System_ceil)||(splay_Search_tcb(OSTCBCurPtr,p_mutex->ID,splayRoot) == 1))
    {    // Resource available? - If splay tree has no entry, mutex is available.
        p_mutex->OwnerTCBPtr       =  OSTCBCurPtr;          // Yes, caller may proceed  
        p_mutex->OwnerOriginalPrio =  OSTCBCurPtr->Prio;
        p_mutex->OwnerNestingCtr   = (OS_NESTING_CTR)1;
        if (p_ts != (CPU_TS *)0) {
           *p_ts                   = p_mutex->TS;
        }
        *flag=1;
        
        splayRoot = splay_Insert(p_mutex, p_mutex->OwnerTCBPtr, splayRoot);
        CPU_CRITICAL_EXIT();
        *p_err                     =  OS_ERR_NONE;
        return;
    }
    else
    {
        OS_CRITICAL_ENTER_CPU_CRITICAL_EXIT();     
       Update_Prio(splayRoot,p_mutex->RCeil,p_mutex->ID,OSTCBCurPtr->Prio);       
       OS_RdyListRemove(OSTCBCurPtr);
       if (*flag==0)
       { 
         avlRoot = avlInsert(avlRoot, p_mutex, OSTCBCurPtr);
         *flag=2;
       }
       *p_err = OS_ERR_MUTEX_OWNER;
       OS_CRITICAL_EXIT_NO_SCHED();
       OSSched();
       return;
    }
}  
                                   
/*$PAGE*/
/*
************************************************************************************************************************
*                                                   POST TO A MUTEX
*
* Description: This function signals a mutex
* Arguments  : p_mutex  is a pointer to the mutex
*              opt      is an option you can specify to alter the behavior of the post.  The choices are:
*                           OS_OPT_POST_NONE        No special option selected
*                           OS_OPT_POST_NO_SCHED    If you don't want the scheduler to be called after the post.
*              p_err    is a pointer to a variable that will contain an error code returned by this function.
*                           OS_ERR_NONE             The call was successful and the mutex was signaled.
*                           OS_ERR_MUTEX_NESTING    Mutex owner nested its use of the mutex
*                           OS_ERR_MUTEX_NOT_OWNER  If the task posting is not the Mutex owner
*                           OS_ERR_OBJ_PTR_NULL     If 'p_mutex' is a NULL pointer.
*                           OS_ERR_OBJ_TYPE         If 'p_mutex' is not pointing at a mutex
*                           OS_ERR_POST_ISR         If you attempted to post from an ISR
* Returns    : none
************************************************************************************************************************
*/
void  OSPCPMutexPost (OS_PCP_MUTEX  *p_mutex,
                      OS_OPT         opt,
                      OS_ERR        *p_err)
{
    OS_PEND_LIST  *p_pend_list;
    OS_TCB        *p_tcb;
    CPU_TS         ts;
    CPU_SR_ALLOC();
     CPU_INT32U NewPrio;
#ifdef OS_SAFETY_CRITICAL
    if (p_err == (OS_ERR *)0) {
        OS_SAFETY_CRITICAL_EXCEPTION();
        return;
    }
#endif

#if OS_CFG_CALLED_FROM_ISR_CHK_EN > 0u
    if (OSIntNestingCtr > (OS_NESTING_CTR)0) {              /* Not allowed to call from an ISR                        */
       *p_err = OS_ERR_POST_ISR;
        return;
    }
#endif

#if OS_CFG_ARG_CHK_EN > 0u
    if (p_mutex == (OS_PCP_MUTEX *)0) {                         /* Validate 'p_mutex'                                     */
        *p_err = OS_ERR_OBJ_PTR_NULL;
        return;
    }
#endif

#if OS_CFG_OBJ_TYPE_CHK_EN > 0u
    if (p_mutex->Type != OS_OBJ_TYPE_MUTEX) {               /* Make sure mutex was created                            */
        *p_err = OS_ERR_OBJ_TYPE;
        return;
    }
#endif
    
    CPU_CRITICAL_ENTER();
    if (OSTCBCurPtr != p_mutex->OwnerTCBPtr) {              /* Make sure the mutex owner is releasing the mutex       */
        CPU_CRITICAL_EXIT();
        *p_err = OS_ERR_MUTEX_NOT_OWNER;
        return;
    }

    OS_CRITICAL_ENTER_CPU_CRITICAL_EXIT();
    ts          = OS_TS_GET();                              /* Get timestamp                                          */
    p_mutex->TS = ts;
    p_mutex->OwnerNestingCtr--;                             /* Decrement owner's nesting counter                      */
    if (p_mutex->OwnerNestingCtr > (OS_NESTING_CTR)0) 
    {    
         OS_CRITICAL_EXIT();                                 
        *p_err = OS_ERR_MUTEX_NESTING;
         return;
    }
    splay_node* sp;
    splayRoot= splay_Delete(p_mutex->ID, splayRoot);
    sp=splay_SearchMinNode(splayRoot);
    System_ceil=  (sp->p_mutex->RCeil);
    if (avl_Search_mutex(p_mutex, avlRoot)==0)
    {
       p_mutex->OwnerTCBPtr     = (OS_TCB       *)0;       /* No                                                     */
       p_mutex->OwnerNestingCtr = (OS_NESTING_CTR)0;
       OS_CRITICAL_EXIT();
      *p_err = OS_ERR_NONE;
       return;
    }
    else
    { 
       avl_node* ptr;
       avl_node* ptr1; 
       CPU_INT32U highPrio=256u;
       ptr= avlFind(avlRoot,p_mutex->ID); 
       //remove the highest priority node in the ready list  
       while (ptr->isTail!=1 && (ptr->p_tcb->Prio >System_ceil))
       { 
           ptr1=ptr;  
           ptr=ptr->next;
       }
       if (ptr->isTail==1)
       {                
           if (ptr->p_tcb->Prio <System_ceil)
           {
                OS_PrioInsert(ptr->p_tcb->Prio);
                OS_RdyListInsertTail(ptr->p_tcb);
                if (ptr->isHead==1)
                { 
                    avlRoot= remove_item(avlRoot,p_mutex->ID);
                }
                else
                {
                    ptr1->next=NULL;
                    ptr1->isTail=1;
                    avl_node* avl;
                    avl= avlFind(avlRoot,p_mutex->ID);
                    sp=splay_search_mutex(p_mutex->RCeil,splayRoot);
                    while(avl->isTail!=1 )
                    {
                        avl->p_mutex=sp->p_mutex;
                        avl=avl->next;
                    }
                    avl->p_mutex=sp->p_mutex;                         
                }
          } 
          else
          {
              avl_node* avl;
              avl= avlFind(avlRoot,p_mutex->ID);
              sp=splay_search_mutex(p_mutex->RCeil,splayRoot);
              while(avl->isTail!=1 )
              {
                 avl->p_mutex=sp->p_mutex;
                 avl=avl->next;
              }
              avl->p_mutex=sp->p_mutex;                   
          }                     
      }
      else
      { 
          avl_node* temp=ptr;
          while(temp->isTail!=1)
          {
             OS_PrioInsert(temp->p_tcb->Prio);
             OS_RdyListInsertTail(temp->p_tcb);
             temp=temp->next;
          }
          OS_PrioInsert(temp->p_tcb->Prio);
          OS_RdyListInsertTail(temp->p_tcb);
          ptr->next=NULL;
      }             
      NewPrio=Calculate_New_Prio( OSTCBCurPtr,splayRoot);
      OS_RdyListRemove(OSTCBCurPtr);
      if (NewPrio==256u)
      {
         OSTCBCurPtr->Prio =  OSTCBCurPtr->OriginalPrio;
      }
      else
         OSTCBCurPtr->Prio=NewPrio;
      
      OS_PrioInsert(OSTCBCurPtr->Prio);
      OS_RdyListInsertTail(OSTCBCurPtr);
      OS_CRITICAL_EXIT_NO_SCHED();
      OSSched();                  
   }   
}